# Changelog

## 1.0.2 (2024-07-18)

* Support for `ipfabric>=6.9.0`

## 1.0.1 (2024-05-06)

* Support for `ipfabric>=6.8.0`

## 1.0.0 (2024-04-17)

* Final cleanup

## 0.2.6 (2024-04-16)

### Feature

* Added Palo Alto OS Command Injection Vulnerability in GlobalProtect [CVE-2024-3400 PAN-OS](https://security.paloaltonetworks.com/CVE-2024-3400)
* Updated affected versions for Cisco [CVE-2023-20198](https://sec.cloudapps.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-iosxe-webui-privesc-j22SaA4z)


## 0.2.5 (2024-04-01)

### Feature

* Added Cisco NETCONF [CVE-2024-20278](https://sec.cloudapps.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-iosxe-priv-esc-seAx6NLX) 

## 0.2.4 (2023-12-15)

### Feature

* Lots of restructuring to allow for passing more inputs via the CLI
* Added the following `pytest.mark`:
  * `cve`
  * `cisco`
* Added `snapshot` option for CLI to select IP Fabric snapshot to use