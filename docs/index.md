---
description: Overview of Python Network Checker, what it contains and how to use it. Python Network Checker is a simple tool for checking the compliance of a device using multiple data sources.
---

# Python Network Checker

Python Network Checker is a simple tool for checking the compliance of a device using multiple data sources.

## Core Dependencies

```
python = "^3.8.1"
ipfabric = "^6.7.0"
ciscoconfparse = "^1.9.0"
pytest = "^8.0.0"
pytest-html-reporter = "^0.2.9"
netutils = "^1.8.0"
```

Dependencies are managed using `poetry`. For a complete list of dependencies and their versions, please refer to the `pyproject.toml` file.

## Why Is This Project Needed?

### Network Compliance, Assurance & Validation

Network compliance (sometimes referred to as "assurance" or "validation") is becoming a more important part of network operations. Network compliance allows you to proactively check if the network is behaving in the intended state you expect. This is especially important in a world where network automation is becoming more prevalent.
With automated deployments and changes to the network, you need to ensure that the network is in a healthy state before or after making changes.

### Extending the IP Fabric Platform

Delivering custom network assurance is a key part of the IP Fabric platform. The platform provides a rich set of data that can be used to create custom tests called "intent checks". 
If a specific technology, vendor, or configuration item is not supported out of the box by IP Fabric, you can use Python Network Checker to create custom tests to check for the presence of these configurations. 

### Reaching the Masses

At IP Fabric, we want everyone to have access to the assurance they need to ensure their network is in a healthy state. With Python Network Checker, we are giving you the ability to provide your own configuration data by passing a directory of configuration files. 
This means that you do not need to be an IP Fabric customer to use Python Network Checker. You can use it to check the compliance of your network devices, regardless of the data source.

## Architecture

Built on top of `pytest` and `pytest-html-exporter`, Python Network Checker is a tool for checking the compliance of a network device.
Using the built-in test cases (or creating your own), the tool is designed to be extensible and easy to use.

### What Type of Data Is Needed?

Any data can be used for checking the compliance of a network device. This could be the running configuration of a device, the state table of a device, or even the output of a command from a device.
Creating a new model and `pytest` fixture to get the data from the source is all that you need to get started.

#### Data Needed for Built-In Test Cases

The built-in test cases use the IP Fabric platform to get the configuration and state table information from the network devices discovered during an IP Fabric discovery.
Not all tests use both the configuration and state table information; some tests only use the configuration, and some only use the state table information.

You can also use the built-in test cases for checking the compliance of a device using a predefined directory of configuration files. This is useful if you are not an IP Fabric customer or if you want to use your own configuration data.
Please note that any tests that require table information from IP Fabric will not work if you do not have access to the IP Fabric platform.

### Why `pytest`?

`pytest` is a mature, full-featured, actively maintained, and widely used Python testing framework. 
When performing network compliance, you have a set of requirements that you want to ascertain are present on the network infrastructure.
You can use `pytest` to define these requirements as `pytest` test cases and then run these tests against the individual network devices on the infrastructure.

In this example, you can see how the basic constructs of `pytest` are utilized to define a test case. 
In this example, we are checking if the SCP Server is enabled on a network device. We are not only looking at the device's running configuration but also its startup configuration.

```python
def test_scp_server(self, device: IPFDevice):
    """Set SCP_SERVER=DISABLED to fail if SCP Server is disabled."""
    check, status = self.return_status("CISCO_SCP_SERVER")

    assert (
        "enabled"
        if device.parsed_config.current.find_lines("^ip scp server enable")
        else "disabled" == check
    ), f"Running Config - SCP Server {status}"
    if device.config.status != "saved" and device.parsed_config.startup:
        assert (
            "enabled"
            if device.parsed_config.startup.find_lines("^ip scp server enable")
            else "disabled" == check
        ), f"Startup Config - SCP Server {status}"
```

#### The Basics; a Test Case for Network Assurance

* `test_scp_server` is the name of the test case. This is the name displayed in the test results.
* `device: IPFDevice` is a [`pytest` fixture](https://docs.pytest.org/en/latest/how-to/fixtures.html#how-to-fixtures) for getting the device configuration and state table information.

The first line of the Python code in the test case is a method for checking the current environment variables to see if the test should fail if the SCP Server is disabled or enabled:

```python
check, status = self.return_status("CISCO_SCP_SERVER")
```

Setting the `CISCO_SCP_SERVER` environment variable to `DISABLED` will cause the test to fail if the SCP Server is disabled:

```shell
$ cat .env
CISCO_SCP_SERVER=DISABLED
```

#### The Test Case Itself

```python
assert (
    "enabled"
    if device.parsed_config.current.find_lines("^ip scp server enable")
    else "disabled" == check
), f"Running Config - SCP Server {status}"
```

After parsing the current configuration of the device, the parsed configuration can be used to check if the SCP Server is enabled or disabled. Based on the environment variable, the test will fail if the SCP Server is not in the state defined by the environment variable.

#### A More Complex Example

In the basic example, we only included data from the configuration file of a network device, examining both the running and saved configuration. A more complex test case may involve not only configuration data but also requiring the state table information from the network device.
Let's review a more complex example of a test case that requires both configuration and state table information.

These requirements may relate to simple configuration checks using the standard output of a network device configuration. You may have more complex requirements,
such as checking the state of the network by referencing the output of a specific command from a network device. Then you can use the IP Fabric API to get the state table information, such as ACL's, ZoneFirewall's, or Route's.

Lets Break down this complex example:
```python
def test_management_information_flow_control(self, device: IPFDevice):
    # Look at Line VTY ACLs using config parser
    # Look up ACL on ACL Table in IPF. If ACL is not found, fail.
    # provide ACL address and name
    access_class_name = None
    vty_lines = self.get_config_section(device.parsed_config.current, r'^line vty \d+ \d+$')
    for line in vty_lines:
        access_class_name = line.re_search_children(r'access-class\s(\w+)')
        if access_class_name:
            access_class_name = access_class_name[0].text
            device_filter = {"policyName": ["like", f"{access_class_name}"]}
            access_lists = device.ipf.fetch_all("technology/security/access-list", filters=device_filter)
            for access_list in access_lists:
                assert 'any' not in access_list['ipSrc'], f"VTY Line configured with ACL that allows any IP address. ACL Name = {access_class_name}"
    assert access_class_name, "VTY Line not configured with ACL"

```
First, we get the VTY lines from the configuration of the device:

```python
vty_lines = self.get_config_section(device.parsed_config.current, r'^line vty \d+ \d+$')
```

Then we look for the access-class name in the VTY lines:

```python
if access_class_name:
    access_class_name = access_class_name[0].text
```

Using the access-class name, we then get the access-list information from the state table of the device. Using the IP Fabric API to get the access-list information:

```python
device_filter = {"policyName": ["like", f"{access_class_name}"]}
access_lists = device.ipf.fetch_all("technology/security/access-list", filters=device_filter)
```

Looping over all the access lists, we then check if any of the access-lists allow an "any" IP address.
```python
for access_list in access_lists:
    assert 'any' not in access_list['ipSrc'], f"VTY Line configured with ACL that allows any IP address. ACL Name = {access_class_name}"
```
This specific assurance test needs to not only confirm the access-class name is present in the VTY lines but also that the access-lists do not allow an 'any' IP address rule.
To confirm, the pressence of an ACL, we assert that an access-class name was found in the VTY lines:

```python
assert access_class_name, "VTY Line not configured with ACL"
```

### Why `pytest-html-reporter`?

`pytest-html-reporter` is a plugin for `pytest` that generates an HTML report for the test results. This is a great way to visualize the results of the tests and to share them with others.
This also allows you to export the results to a file and then use that file to generate a report or to integrate the results into another system.

## Installation

To install Python Network Checker, please follow the instructions in the "Installation" section of the repository's [README.md](https://gitlab.com/ip-fabric/integrations/pynetcheck#installation) file.

## Executing Test

To execute a test, please follow the instructions in the "Running" section of the repository's [README.md](https://gitlab.com/ip-fabric/integrations/pynetcheck#running) file.

## Filtering Tests

Pytest Marks have been added to allow for filtering of tests. Please see the readme file section "Filtering" for more information. [README.md](https://gitlab.com/ip-fabric/integrations/pynetcheck#filtering)